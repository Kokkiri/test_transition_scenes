extends Node

onready var progress = get_node("fond/progress")

var loader
var wait_frames
var time_max = 100 # msec


func _ready():
#	var root = get_tree().get_root()
#	current_scene = root.get_child(root.get_child_count() -1)
	print('ready')
	var menu = "res://Intro.tscn"
	goto_scene(menu)
	

func goto_scene(path): # Game requests to switch to this scene.
	print('goto_scene')
	loader = ResourceLoader.load_interactive(path)
	if loader == null: # Check for errors.
#        show_error()
		return
	set_process(true)
	print('process is true')
#	print('current_scene ', current_scene.get_name())
#	current_scene.queue_free() # Get rid of the old scene.
	print('start_animation')
	get_node("fond/animation").play("loading")
	wait_frames = 1

func _process(_time):
	if loader == null:
		# no need to process anymore
		set_process(false)
		print('process is false')
		return

	# Wait for frames to let the "loading" animation show up.
	if wait_frames > 0:
		wait_frames -= 1
		return

	var t = OS.get_ticks_msec()
	# Use "time_max" to control for how long we block this thread.
	while OS.get_ticks_msec() < t + time_max:
		var err = loader.poll()
		if err == ERR_FILE_EOF: # Finished loading.
			print('finish_loading')
			progress.value = 100
			var resource = loader.get_resource()
			loader = null
			set_new_scene(resource)
			break
		elif err == OK:
			update_progress()
		else: # Error during loading.
#            show_error()
			loader = null
			break
	print('fin de process')

func update_progress():
	var progress_value = (loader.get_stage() * 100) / loader.get_stage_count()
	progress.set_value(progress_value)
	
func replace_main_scene(resource):
	print('replace_main_scene')
	call_deferred("set_new_scene", resource)

func set_new_scene(scene_resource):
	print('set_new_scene')
	var node = scene_resource.instance()
	print('node ', node.get_name())
	
	for child in get_children():
		print(child)
		remove_child(child)
		child.queue_free()
	add_child(node)

	node.connect("quit", self, "replace_main_scene")
	node.connect("change_scene", self, "replace_main_scene")
