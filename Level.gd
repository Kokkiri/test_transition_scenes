extends Node

signal quit

func _ready():
	$jupiter.play()

func _on_quit_pressed():
	var main = ResourceLoader.load("res://Main.tscn")
	emit_signal('quit', main)
	print('quit from level')
