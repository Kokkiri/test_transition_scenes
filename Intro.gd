extends Node

signal change_scene

func _ready():
	$patio.play()

func _input(event):
	if event.is_action_pressed('quit'):
		get_tree().quit()
		print('QUIT')

func _on_start_pressed():
	var path = "res://Level.tscn"
	var level = ResourceLoader.load(path)
#		loading_thread = Thread.new()
#		#warning-ignore:return_value_discarded
#		loading_thread.start(self, "interactive_load", res_loader)
	emit_signal('change_scene', level)
	print('from intro to level')

func _on_quit_pressed():
		get_tree().quit()
		print('QUIT')
